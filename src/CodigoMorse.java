
public class CodigoMorse {

    public static String conversaoAlfabetoToMorse(String palavra) {
        String solucao = "";
        int repeticoes = palavra.length();
        for (int i = 0; i < repeticoes; i++) {
            char letra = palavra.charAt(i);
            switch (letra) {
                case 'A':
                    solucao = solucao + "·-";
                    break;
                case 'B':
                    solucao = solucao + "-···";
                    break;
                case 'C':
                    solucao = solucao + "-·-·";
                    break;
                case 'D':
                    solucao = solucao + "-..";
                    break;
                case 'E':
                    solucao = solucao + "·";
                    break;
                case 'F':
                    solucao = solucao + "..-.";
                    break;
                case 'G':
                    solucao = solucao + "--.";
                    break;
                case 'H':
                    solucao = solucao + "....";
                    break;
                case 'I':
                    solucao = solucao + "..";
                    break;
                case 'J':
                    solucao = solucao + "·---";
                    break;
                case 'K':
                    solucao = solucao + "-·-";
                    break;
                case 'L':
                    solucao = solucao + ".-..";
                    break;
                case 'M':
                    solucao = solucao + "--";
                    break;
                case 'N':
                    solucao = solucao + "-.";
                    break;
                case 'O':
                    solucao = solucao + "---";
                    break;
                case 'P':
                    solucao = solucao + "-.-.";
                    break;
                case 'Q':
                    solucao = solucao + "--.-";
                    break;
                case 'R':
                    solucao = solucao + ".-.";
                    break;
                case 'S':
                    solucao = solucao + "...";
                    break;
                case 'T':
                    solucao = solucao + "-";
                    break;
                case 'U':
                    solucao = solucao + "..-";
                    break;
                case 'V':
                    solucao = solucao + "...-";
                    break;
                case 'W':
                    solucao = solucao + ".--";
                    break;
                case 'X':
                    solucao = solucao + "-..-";
                    break;
                case 'Y':
                    solucao = solucao + "-.--";
                    break;
                case 'Z':
                    solucao = solucao + "--..";
                    break;
                // ---------------------------- NÚMEROS ---------------
                case '0':
                    solucao = solucao + "-----";
                    break;
                case '1':
                    solucao = solucao + ".----";
                    break;
                case '2':
                    solucao = solucao + "..---";
                    break;
                case '3':
                    solucao = solucao + "...--";
                    break;
                case '4':
                    solucao = solucao + "....-";
                    break;
                case '5':
                    solucao = solucao + ".....";
                    break;
                case '6':
                    solucao = solucao + "-....";
                    break;
                case '7':
                    solucao = solucao + "--...";
                    break;
                case '8':
                    solucao = solucao + "---..";
                    break;
                case '9':
                    solucao = solucao + "----.";
                    break;
                // -----------------  CARACTERES ESPECIAIS ----------------
                case ' ':
                    solucao = solucao + "/";
                    break;
                case '.':
                    solucao = solucao + ".-.-.-";
                    break;
                case ',':
                    solucao = solucao + "--..--";
                    break;
                case '?':
                    solucao = solucao + "..--..";
                    break;
                case '\'':
                    solucao = solucao + ".----.";
                    break;
                case '!':
                    solucao = solucao + "-.-.--";
                    break;
                case '/':
                    solucao = solucao + "-..-.";
                    break;
                case '(':
                    solucao = solucao + "-.--.";
                    break;
                case ')':
                    solucao = solucao + "-.--.-";
                    break;
                case '&':
                    solucao = solucao + "....";
                    break;
                case ':':
                    solucao = solucao + "---...";
                    break;
                case ';':
                    solucao = solucao + "-.-.-.";
                    break;
                case '=':
                    solucao = solucao + "-...-";
                    break;
                case '+':
                    solucao = solucao + ".-.-.";
                    break;
                case '-':
                    solucao = solucao + "-....-";
                    break;
                case '_':
                    solucao = solucao + "..--.-";
                    break;
                case '"':
                    solucao = solucao + ".-..-.";
                    break;
                case '$':
                    solucao = solucao + "...-..-";
                    break;
                case '@':
                    solucao = solucao + ".--.-.";
                    break;
                //----- LETRAS ESPECIAIS ------
                case 'Ä':
                    solucao = solucao + ".-.-";
                    break;
                case 'Æ':
                    solucao = solucao + ".-.-";
                    break;
                case 'À':
                    solucao = solucao + ".--.-";
                    break;
                case 'Á':
                    solucao = solucao + ".--.-";
                    break;
                case 'Å':
                    solucao = solucao + ".--.-";
                    break;
                case 'Ç':
                    solucao = solucao + "-.-..";
                    break;
                case 'Ĉ':
                    solucao = solucao + "-.-..";
                    break;
                case 'Š':
                    solucao = solucao + "----";
                    break;
                case 'Ð':
                    solucao = solucao + "..--.";
                    break;
                case 'È':
                    solucao = solucao + ".-..-";
                    break;
                case 'É':
                    solucao = solucao + "..-..";
                    break;
                case 'Ĝ':
                    solucao = solucao + "--.-.";
                    break;
                case 'Ĥ':
                    solucao = solucao + "----";
                    break;
                case 'Ĵ':
                    solucao = solucao + ".---.";
                    break;
                case 
                    'Ñ'
                    :
                    solucao = solucao + "--.--";
                    break;
                case 
                    'Ö'
                    :
                    solucao = solucao + "---.";
                    break;
                case 
                    'Ø'
                    :
                    solucao = solucao + "---.";
                    break;
                case 
                    'Ŝ'
                    :
                    solucao = solucao + "...-.";
                    break;
                case 
                    'Þ'
                    :
                    solucao = solucao + ".--..";
                    break;
                case 
                    'Ü'
                    :
                    solucao = solucao + "..--";
                    break;
                case 
                    'Ŭ'
                    :
                    solucao = solucao + "..--";
                    break;
                //----- ELSE ------
                default:
                    solucao = solucao + "Error";
                    break;
            }
            solucao = solucao + "  ";
        }
        return solucao;
    }

    public static String conversaoMorsetoAlfabeto(String codigo) {
        return "Em desenvolvimento";
    }
}
